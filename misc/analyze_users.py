#!/usr/bin/env python3

"""
A script which analyzes a file of the following format and returns the Canvas usernames
of those listed along with NUMBER:

   NUMBER CANVAS_ID
  NUMBER CANVAS_ID

The spacing before NUMBER is arbitrary. The script outputs a CSV containing the
following information:

E-mail,Views
"""

import csv
from os import environ
from sys import argv
from datetime import datetime
import re
from urllib import parse

from canvasapi import Canvas
from canvasapi.exceptions import ResourceDoesNotExist

canvas = Canvas(environ["API_URL"], environ["API_KEY"])


class User:
    """Represents a user of the app"""

    def __init__(self, id):
        """Given this user's id, fills out `sortable_name`, `email`, and canvas_user"""

        try:
            self.canvas_user = canvas.get_user(id)
            self.email = self.canvas_user.email
            self.sortable_name = self.canvas_user.sortable_name
        except ResourceDoesNotExist:
            self.email = "unknown"
            self.sortable_name = "Unknown User"

        self.views = []

    id = 0
    """This user's ID in Canvas"""

    views = None
    """A list of datetimes that this user has viewed the app"""

    email = ""
    """The user's primary e-mail in Canvas"""

    sortable_name = ""
    """The user's sorable name in Canvas"""

    canvas_user = None
    """The user's Canvas object"""

class View:
    """Represents a single page hit (AKA "app use" or "view")"""

    def __init__(self, course, time):
        self.course = course
        self.time = time

    course = 0
    """The course that this view occurred on"""

    time = None
    """Datetime object representing when this view occurred"""

    def __lt__(self, other):
        return self.time < other.time

    def __eq__(self, other):
        return (self.time == other.time) and (self.course == other.course)

def get_user(users, id):
    """Gets a user entry from the dict ``users``, creates it if it is not found"""
    user = users.get(id)
    if not user:
        user = User(id)
        users[id] = user

    return user


users = {}
try:
    with open(argv[1]) as logfile:
        lines = logfile.readlines()
        num_lines = len(lines)
        current_line = 0
        print("File opened and read, retrieving information...")
        for line in lines:
            current_line += 1
            fields = line.replace("[", "").replace("]", "").split()
            tz = fields[1]
            datetime_str = fields[0] + tz
            view_time = datetime.strptime(datetime_str, "%d/%b/%Y:%H:%M:%S%z")

            url = fields[3]
            query_vars = parse.parse_qs(parse.urlparse(url).query)
            user_id = query_vars.get("user_id", [0])[0]
            course_id = query_vars.get("course_id", ["Invalid"])[0]

            view = View(course_id, view_time)

            user = get_user(users, user_id)
            user.views.append(view)
            current_percent = round(((current_line / num_lines) * 100), 0)
            print(str(current_percent) + "%", end="\r")
except FileNotFoundError:
    print("Requested file was not found.")
    exit(1)

print("100.0%\nDone, writing report.")

with open("users.csv", "w") as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(["Name", "Email", "Views", "First view", "Last view", "Courses viewed"])
    out_timestring = "%Y/%m/%d %H:%M:%S"
    current_user = 0
    total_users = len(users)
    for (user_id, user) in users.items():
        first_view = min(user.views)
        last_view = max(user.views)
        view_count = len(user.views)
        courses = [view.course for view in user.views]

        csvwriter.writerow(
            [
                user.sortable_name,
                user.email,
                view_count,
                first_view.time.strftime(out_timestring),
                last_view.time.strftime(out_timestring),
                ', '.join(courses)
            ]
        )

        current_percent = round(((current_user / total_users) * 100), 0)
        print(str(current_percent) + "%", end="\r")

print("100.0%\nDone. The report can be found at 'users.csv' in your current directory.")
