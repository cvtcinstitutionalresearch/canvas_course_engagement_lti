import logging
import time
from datetime import timedelta
from functools import wraps
from logging.handlers import RotatingFileHandler
from os import environ
import json

import requests

import settings
from flask import (
    Flask,
    Response,
    redirect,
    render_template,
    request,
    session,
    url_for,
    make_response,
)
from flask_sqlalchemy import SQLAlchemy
from pylti.flask import lti
from canvas_course_engagement.student_engagement import StudentRating

app = Flask(__name__)
app.secret_key = settings.secret_key
app.config.from_object(settings.configClass)
db = SQLAlchemy(app)

# Logging
formatter = logging.Formatter(settings.LOG_FORMAT)
handler = RotatingFileHandler(
    settings.LOG_FILE,
    maxBytes=settings.LOG_MAX_BYTES,
    backupCount=settings.LOG_BACKUP_COUNT,
)
handler.setLevel(logging.getLevelName(settings.LOG_LEVEL))
handler.setFormatter(formatter)
app.logger.addHandler(handler)


def check_environment():

    try:
        API_URL = environ["CCE_API_URL"]
    except KeyError:
        app.logger.critical(
            "CCE_API_URL not found in environment. Please add a URL to the CCE backend API."
        )
        exit(1)

    if "://" not in API_URL:
        app.logger.critical(
            "Protocol information not found in CCE_API_URL in environment. Please add a protocol (such as 'http://') to the environment variable."
        )
        exit(1)

    app.config["CCE_API_URL"] = API_URL


app.before_first_request(check_environment)

# DB Model
class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, unique=True)
    refresh_key = db.Column(db.String(255))
    expires_in = db.Column(db.BigInteger)

    def __init__(self, user_id, refresh_key, expires_in):
        self.user_id = user_id
        self.refresh_key = refresh_key
        self.expires_in = expires_in

    def __repr__(self):
        return "<User %r>" % self.user_id


# Template model
class Student:
    def __init__(self, new_json):
        self.user_id = new_json["user_id"]
        self.engagement = new_json["engagement"]
        self.name = new_json["user"]["name"]
        self.sortable_name = new_json["user"]["sortable_name"]
        self.grade = new_json["grade"]
        self.page_views = new_json["page_views"]
        self.page_view_percentile = new_json["page_view_percentile"]

    def __str__(self):
        return "<Student {}>".format(self.user_id)

    def __repr__(self):
        return "<Student {}, Engagement: {}>".format(self.user_id, self.engagement)


# Utility Functions
def return_error(msg):
    return make_response(render_template("error.htm", msg=msg), 400)


def error(exception=None):
    app.logger.debug(session)
    app.logger.error("PyLTI error: {}".format(exception))
    return return_error(
        """Authentication error,
        please refresh and try again. If this error persists,
        please contact support."""
    )


def redirect_to_auth():
    """Redirects the user to the Canvas OAUTH flow

    This function uses BASE_URL and the oauth settings from settings.py to redirect the
    user to the appropriate place in their Canvas installation for authentication.
    """
    return redirect(
        "{}login/oauth2/auth?client_id={}&response_type=code&redirect_uri={}&scope={}".format(
            settings.BASE_URL,
            settings.oauth2_id,
            settings.oauth2_uri,
            settings.oauth2_scopes,
        )
    )


def check_valid_user(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        """
        Decorator to check if the user is allowed access to the app.

        If user is allowed, return the decorated function.
        Otherwise, return an error page with corresponding message.
        """
        if request.form:
            session["course_id"] = request.form.get("custom_canvas_course_id")
            session["canvas_user_id"] = request.form.get("custom_canvas_user_id")
            roles = request.form["roles"]

            if "Administrator" in roles:
                session["admin"] = True
                session["instructor"] = True
            elif "admin" in session:
                # remove old admin key in the session
                session.pop("admin", None)

            if "Instructor" in roles:
                session["instructor"] = True
            elif "instructor" in session:
                # remove old instructor key from the session
                session.pop("instructor", None)

        # no session and no request
        if not session:
            if not request.form:
                app.logger.warning("No session and no request. Not allowed.")
                return return_error("No session or request provided.")

        # no canvas_user_id
        if (
            not request.form.get("custom_canvas_user_id")
            and "canvas_user_id" not in session
        ):
            app.logger.warning("No canvas user ID. Not allowed.")
            return return_error("No canvas uer ID provided.")

        # no course_id
        if (
            not request.form.get("custom_canvas_course_id")
            and "course_id" not in session
        ):
            app.logger.warning("No course ID. Not allowed.")
            return return_error("No course_id provided.")

        # If they are neither instructor or admin, they're not in the right place

        if "instructor" not in session and "admin" not in session:
            app.logger.warning("Not enrolled as Teacher or an Admin. Not allowed.")
            return return_error(
                """You are not enrolled in this course as a Teacher or Designer.
            Please refresh and try again. If this error persists, please contact support."""
            )

        return f(*args, **kwargs)

    return decorated_function


def refresh_access_token(user):
    """
    Use a user's refresh token to get a new access token.

    :param user: The user to get a new access token for.
    :type user: :class:`Users`

    :rtype: dict
    :returns: Dictionary with keys 'access_token' and 'expiration_date'.
        Values will be `None` if refresh fails.
    """
    refresh_token = user.refresh_key

    payload = {
        "grant_type": "refresh_token",
        "client_id": settings.oauth2_id,
        "redirect_uri": settings.oauth2_uri,
        "client_secret": settings.oauth2_key,
        "refresh_token": refresh_token,
    }
    response = requests.post(settings.BASE_URL + "login/oauth2/token", data=payload)

    try:
        response_json = response.json()
    except json.decoder.JSONDecodeError:
        # The response had no JSON, it might have been empty due to an auth
        # problem.
        return {"access_token": None, "expiration_date": None}

    if "access_token" not in response_json:
        app.logger.warning(
            (
                "Access token not in json. Bad api key or refresh token.\n"
                "URL: {}\n"
                "Status Code: {}\n"
                "Payload: {}\n"
                "Session: {}"
            ).format(response.url, response.status_code, payload, session)
        )
        return {"access_token": None, "expiration_date": None}

    api_key = response_json["access_token"]
    app.logger.debug("New access token created\n User: {0}".format(user.user_id))

    if "expires_in" not in response_json:
        app.logger.warning(
            (
                "expires_in not in json. Bad api key or refresh token.\n"
                "URL: {}\n"
                "Status Code: {}\n"
                "Payload: {}\n"
                "Session: {}"
            ).format(response.url, response.status_code, payload, session)
        )
        return {"access_token": None, "expiration_date": None}

    current_time = int(time.time())
    new_expiration_date = current_time + response_json["expires_in"]

    # Update expiration date in db
    user.expires_in = new_expiration_date
    db.session.commit()

    # Confirm that expiration date has been updated
    updated_user = Users.query.filter_by(user_id=int(user.user_id)).first()
    if updated_user.expires_in != new_expiration_date:
        readable_expires_in = time.strftime(
            "%a, %d %b %Y %H:%M:%S", time.localtime(updated_user.expires_in)
        )
        readable_new_expiration = time.strftime(
            "%a, %d %b %Y %H:%M:%S", time.localtime(new_expiration_date)
        )
        app.logger.error(
            (
                "Error in updating user's expiration time in the db:\n"
                "session: {}\n"
                "DB expires_in: {}\n"
                "new_expiration_date: {}"
            ).format(session, readable_expires_in, readable_new_expiration)
        )
        return {"access_token": None, "expiration_date": None}

    return {"access_token": api_key, "expiration_date": new_expiration_date}


def sort_student(student):
    """ Returns a sort tuple for a Student object.
    
    Sorts by page view percentile within grade within engagement. The order of
    engagement ratings is Engage first, Explore second, Encourage last.
    """
    engagement = student.engagement
    if engagement == StudentRating.ENCOURAGE.value:
        engagement = 2
    elif engagement == StudentRating.EXPLORE.value:
        engagement = 1
    elif engagement == StudentRating.ENGAGE.value:
        engagement = 0
    else:
        engagement = 4

    return (engagement, student.grade, student.page_view_percentile)


# Web Views / Routes
@app.route("/index", methods=["GET"])
@lti(error=error, request="session", role="staff", app=app)
@check_valid_user
def index(lti=lti):
    session["course_id"] = request.args.get("course_id", "")
    session["user_id"] = request.args.get("user_id", "")
    app.logger.debug("Got session information")

    headers = {b"Authorization": "Bearer {}".format(session["api_key"]).encode("utf8")}
    url = "{}/courses/{}".format(app.config["CCE_API_URL"], session["course_id"])
    app.logger.debug("Getting info from API: " + url)
    r = requests.get(url, headers=headers)
    app.logger.debug("Got API response")
    if r.status_code == 401:
        app.logger.warning(
            "Got 401 unauthorized from backend, /index?course_id={}".format(
                session["course_id"]
            )
        )
        return return_error("Unauthorized to perform that operation on this course.")
    elif r.status_code != 200:
        app.logger.error("Got {} from backend, {}".format(r.status_code, url))
        return return_error(
            "The Engagement API encountered an error. Please contact this service's administrator."
        )
    r_json = r.json()
    del r
    students = [Student(student) for student in r_json["students"]]
    course_id = r_json["course"]["id"]
    course_name = r_json["course"]["name"]

    if not len(students):
        return render_template("index_empty.htm", course_name=course_name)

    average_grade = round(r_json["average_grade"], 2)
    del r_json
    app.logger.debug("Made student list, sorting.")
    students = sorted(students, key=lambda student: sort_student(student))
    app.logger.debug("Sort done, rendering.")

    return render_template(
        "index.htm",
        students=students,
        course_name=course_name,
        course_id=course_id,
        average_grade=average_grade,
        base_url=settings.BASE_URL
    )


# OAuth login
# Set as OAUTH Redirect URI
@app.route("/oauthlogin", methods=["POST", "GET"])
@lti(error=error, role="staff", app=app)
@check_valid_user
def oauth_login(lti=lti):
    code = request.args.get("code")
    payload = {
        "grant_type": "authorization_code",
        "client_id": settings.oauth2_id,
        "redirect_uri": settings.oauth2_uri,
        "client_secret": settings.oauth2_key,
        "code": code,
    }
    r = requests.post(settings.BASE_URL + "login/oauth2/token", data=payload)
    print("")
    print(r.status_code)
    print(r.text)

    if r.status_code == 500:
        # Canceled oauth or server error
        app.logger.error(
            """Status code 500 from oauth, authentication error\n
            User ID: None Course: None \n {0} \n Request headers: {1} \n Session: {2}""".format(
                r.url, r.headers, session
            )
        )

        msg = """Authentication error,
            please refresh and try again. If this error persists,
            please contact support."""
        return return_error(msg)

    if "access_token" in r.json():
        session["api_key"] = r.json()["access_token"]

        if "refresh_token" in r.json():
            refresh_token = r.json()["refresh_token"]

        if "expires_in" in r.json():
            # expires in seconds
            # add the seconds to current time for expiration time
            current_time = int(time.time())
            expires_in = current_time + r.json()["expires_in"]
            session["expires_in"] = expires_in

            # check if user is in the db
            user = Users.query.filter_by(user_id=int(session["canvas_user_id"])).first()
            if user is not None:
                # update the current user's expiration time in db
                user.refresh_key = refresh_token
                user.expires_in = session["expires_in"]
                db.session.add(user)
                db.session.commit()

                # check that the expires_in time got updated
                check_expiration = Users.query.filter_by(
                    user_id=int(session["canvas_user_id"])
                )

                # compare what was saved to the old session
                # if it didn't update, error
                try:
                    expires_in = int(session["expires_in"])
                except AttributeError:
                    expires_in = 0

                try:
                    database_expires_in = check_expiration.expires_in
                except AttributeError:
                    database_expires_in = -1

                if database_expires_in == expires_in:
                    course_id = session["course_id"]
                    user_id = session["canvas_user_id"]
                    return redirect(
                        url_for("index", course_id=course_id, user_id=user_id)
                    )
                else:
                    app.logger.error(
                        """Error in updating user's expiration time
                        in the db:\n {0}""".format(
                            session
                        )
                    )
                    return return_error(
                        """Authentication error,
                            please refresh and try again. If this error persists,
                            please contact support."""
                    )
            else:
                # add new user to db
                new_user = Users(
                    session["canvas_user_id"], refresh_token, session["expires_in"]
                )
                db.session.add(new_user)
                db.session.commit()

                # check that the user got added
                check_user = Users.query.filter_by(
                    user_id=int(session["canvas_user_id"])
                ).first()

                if check_user is None:
                    # Error in adding user to the DB
                    app.logger.error(
                        "Error in adding user to db: \n {0}".format(session)
                    )
                    return return_error(
                        """Authentication error,
                        please refresh and try again. If this error persists,
                        please contact support."""
                    )
                else:
                    course_id = session["course_id"]
                    user_id = session["canvas_user_id"]
                    return redirect(
                        url_for("index", course_id=course_id, user_id=user_id)
                    )

            # got beyond if/else
            # error in adding or updating db

            app.logger.error(
                "Error in adding or updating user to db: \n {0} ".format(session)
            )
            return return_error(
                """Authentication error,
                please refresh and try again. If this error persists,
                please contact support."""
            )

    app.logger.warning(
        "Some other error\n {0} \n {1} \n Request headers: {2} \n {3}".format(
            session, r.url, r.headers, r.json()
        )
    )
    msg = """Authentication error,
        please refresh and try again. If this error persists,
        please contact support."""
    return return_error(msg)


@app.route("/launch", methods=["POST", "GET"])
@lti(error=error, request="initial", role="staff", app=app)
@check_valid_user
def launch(lti=lti):
    app.logger.debug(session)
    user = Users.query.filter_by(user_id=int(session["canvas_user_id"])).first()

    if not user:
        app.logger.debug(
            "Person doesn't have an entry in db, redirecting to oauth: {0}".format(
                session
            )
        )
        return redirect_to_auth()

    app.logger.debug("They're a user! {}".format(user.user_id))
    # Get the expiration date
    expiration_date = user.expires_in

    # If expired or no api_key
    if int(time.time()) > expiration_date or "api_key" not in session:
        readable_time = time.strftime(
            "%a, %d %b %Y %H:%M:%S", time.localtime(user.expires_in)
        )

        app.logger.debug(
            (
                "Expired refresh token or api_key not in session\n"
                "User: {0}\n"
                "Expiration date in db: {1}\n"
                "Readable expires_in: {2}"
            ).format(user.user_id, user.expires_in, readable_time)
        )

        refresh = refresh_access_token(user)

        if refresh["access_token"] and refresh["expiration_date"]:
            # Success! Set the API Key and Expiration Date
            session["api_key"] = refresh["access_token"]
            session["expires_in"] = refresh["expiration_date"]
            return redirect(
                url_for(
                    "index",
                    course_id=session["course_id"],
                    user_id=session["canvas_user_id"],
                )
            )
        else:
            # Refresh didn't work. Reauthenticate.
            app.logger.debug("Reauthenticating:\nSession: {}".format(session))
            return redirect_to_auth()
    else:
        # Have an API key that shouldn't be expired. Test it to be sure.
        auth_header = {"Authorization": "Bearer " + session["api_key"]}
        r = requests.get(
            "{}users/{}/profile".format(settings.API_URL, session["canvas_user_id"]),
            headers=auth_header,
        )

        # check for WWW-Authenticate
        # https://canvas.instructure.com/doc/api/file.oauth.html
        if "WWW-Authenticate" not in r.headers and r.status_code == 200:
            return redirect(
                url_for(
                    "index",
                    course_id=session["course_id"],
                    user_id=session["canvas_user_id"],
                )
            )
        else:
            # Key is bad. First try to get a new one using refresh
            new_token = refresh_access_token(user)["access_token"]

            if new_token:
                session["api_key"] = new_token
                return redirect(
                    url_for(
                        "index",
                        course_id=session["course_id"],
                        user_id=session["canvas_user_id"],
                    )
                )
            else:
                # Refresh didn't work. Reauthenticate.
                app.logger.debug("Reauthenticating:\nSession: {}".format(session))
                return redirect_to_auth()


# XML
@app.route("/xml/", methods=["GET"])
def xml():
    """
    Returns the lti.xml file for the app.
    XML can be built at https://www.eduappcenter.com/
    """
    try:
        return Response(render_template("lti.xml"), mimetype="application/xml")
    except Exception as e:
        app.logger.exception(e)
        msg = (
            "No XML file. Please refresh and try again. "
            "If this error persists, please contact support."
        )
        return return_error(msg)

@app.template_filter()
def os_getenv(value, key):
  return environ.get(key, default=value)

class ReverseProxied(object):
    """Wrap the application in this middleware and configure the
    front-end server to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    In nginx, for example:
    location /myprefix {
        proxy_pass http://192.168.0.1:5001;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Script-Name /myprefix;
        }

    :param app: the WSGI application
    """

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        script_name = environ.get("HTTP_X_SCRIPT_NAME", "")
        if script_name:
            environ["SCRIPT_NAME"] = script_name
            path_info = environ["PATH_INFO"]
            if path_info.startswith(script_name):
                environ["PATH_INFO"] = path_info[len(script_name) :]

        scheme = environ.get("HTTP_X_FORWARDED_PROTO", "")
        if scheme:
            environ["wsgi.url_scheme"] = scheme
        return self.app(environ, start_response)

app.wsgi_app = ReverseProxied(app.wsgi_app)
